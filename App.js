import React, { Component } from 'react';
import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';

import {Provider} from "react-redux";
import reducer from "./store/reducer";
import Picture from "./container/Picture";

const store = createStore(reducer, applyMiddleware(thunk));

class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <Picture />
            </Provider>
        );
    }
}

export default App;
