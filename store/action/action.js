
import * as actionTypes from './actionTypes';

import axios from "../../axios";

export const orderRequest = () => {
    return {type: actionTypes.ORDER_REQUEST};
};

export const orderSuccess = (value) => {
    return {type: actionTypes.ORDER_SUCCESS, value};
};

export const orderFailure = error => {
    return {type: actionTypes.ORDER_FAILURE, error};
};

export const getPicture = () => {
    return dispatch => {
        dispatch(orderRequest());
        axios.get('/pics.json').then(response => {
            console.log(response.data)
            dispatch(orderSuccess(response.data.data.children.map(item => item.data)));
        }, error => {
            dispatch(orderFailure(error))
        })
    }
};
