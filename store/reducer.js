import * as actionTypes from './action/actionTypes';

const initialState = {
    pictures: []
};

const reducer = (state = initialState, action) => {

    switch (action.type) {
        case actionTypes.ORDER_SUCCESS:
            console.log(action.value)
            return {...state, pictures: action.value};
        default:
            return state;
    }

};

export default reducer;