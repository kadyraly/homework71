import React, {Component} from 'react';
import {View, Text,  StyleSheet, Image} from 'react-native';

class PLaceItem extends Component {

    render () {
        return (
            <View style={styles.placeItem}>
                <Image source={{uri: this.props.thumbnail}} style={{width: 40, height: 40, margin: 10}} />
                <Text>{this.props.title} </Text>
            </View>
        )
    }
};

export default PLaceItem;

const styles = StyleSheet.create({

    placeItem: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        backgroundColor: '#eee',
        marginBottom: 10,
        padding: 10
    }
});