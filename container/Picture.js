import React, {Component} from 'react';

import {connect} from 'react-redux';
import {View, Text, FlatList, StyleSheet} from 'react-native';
import {getPicture} from "../store/action/action";
import PLaceItem from "./PlaceItem";



class Picture extends Component {

    componentDidMount () {
        this.props.onPicGot();
    }



    render () {
        return(
            <View style={styles.container}>
                <FlatList
                    style={styles.item}
                    renderItem={({item}) => {
                        console.log(item, 'ITEM')
                        return <PLaceItem thumbnail={item.thumbnail} title={item.title} />}}
                    keyExtractor={(item, index) => index}
                    data={this.props.pictures} />

            </View>
        )
    }
};

const mapStateToProps = state => {
    return {
        pictures: state.pictures
    };
};
const mapDispatchToProps = dispatch => {
    return {

        onPicGot: () => dispatch(getPicture())

    };
};


export default connect(mapStateToProps, mapDispatchToProps)(Picture);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'flex-start',
        padding: 30
    },
    item: {
        width: '100%'
    }

});